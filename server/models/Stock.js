// Stock model not currently in use

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let Stock = new Schema({
  symbol: {
    type: String
  },
  price: {
    type: Number
  }
});

module.exports = mongoose.model('Stock', Stock);

// [ { "key": "FB", "value": 194.62 }, { "key": "AAPL", "value": 220.51 }, { "key": "MRK", "value": 88.59 }, { "key": "LMT", "value": 397.17 }, { "key": "TSLA", "value": 247.29 } ]
