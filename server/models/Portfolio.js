const mongoose = require('mongoose');
const Schema = mongoose.Schema;


// Define collection and schema for Portfolio
let Portfolio = new Schema(
  {
    user: { type: String, required: true, unique: true },
    stocks: { type: Array },
    date: { type: Date, default: Date.now },

  },
  { timestamps: true },
  { collection: 'portfolio' }
);

module.exports = mongoose.model('Portfolio', Portfolio);
