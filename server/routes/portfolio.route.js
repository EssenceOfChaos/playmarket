const express = require('express');
const app = express();
const portfolioRoutes = express.Router();

// Require Portfolio model in our routes module
let Portfolio = require('../models/Portfolio');

// Defined index route -- list all portfolios
portfolioRoutes.route('/').get(function (req, res) {
  Portfolio.find(function (err, portfolios) {
    if (err) {
      console.error(err);
    } else {
      res.json(portfolios);
    }
  });
});

// Defined insert route
portfolioRoutes.route('/').post(function (req, res) {
  let user = req.body.user
  let stocks = req.body.stocks
  let portfolio = new Portfolio({ user: user, stocks: stocks });

  portfolio
    .save()
    .then(portfolio => {
      console.log(portfolio);
      res.status(201).json({ portfolio: 'portfolio added successfully' });
    })
    .catch(err => {
      res.status(400).send('unable to save to database');
    });
});



// Show Portfolio Route
portfolioRoutes.route('/:user').get(function (req, res) {
  let user = req.params.user
  Portfolio.find({ user: user }, 'stocks', function (err, docs) {
    if (err) {
      console.error(err);
    } else {
      res.json(docs)
    }
  });
});

// Defined edit route
// portfolioRoutes.route('/:id').get(function (req, res) {
//   let id = req.params.id;
//   Portfolio.findById(id, function (err, portfolio) {
//     res.json(portfolio);
//   });
// });

//  Defined update route
portfolioRoutes.route('/:id').put(function (req, res) {
  Portfolio.findById(req.params.id, function (err, next, portfolio) {
    if (!portfolio) return next(new Error('Could not load Document'));
    else {
      portfolio.key = req.body.key;
      portfolio.value = req.body.value;

      portfolio
        .save()
        .then(portfolio => {
          res.json('Update complete');
        })
        .catch(err => {
          res.status(400).send('unable to update the database');
        });
    }
  });
});

// Defined delete | remove | destroy route
portfolioRoutes.route('/:id').delete(function (req, res) {
  Portfolio.findByIdAndRemove({ _id: req.params.id }, function (err, portfolio) {
    if (err) res.json(err);
    else res.json('Successfully removed');
  });
});

module.exports = portfolioRoutes;
