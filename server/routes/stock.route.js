const express = require('express');
const app = express();
const stockRoutes = express.Router();

// Require Stock model in our routes module
let Stock = require('../models/Stock');

// Defined store route
stockRoutes.route('/').post(function (req, res) {
  let stock = new Stock({ key: '', value: 0 });

  stock
    .save()
    .then(stock => {
      res.status(200).json({ stock: 'stock added successfully' });
    })
    .catch(err => {
      res.status(400).send('unable to save to database');
    });
});

// Defined get data(index or listing) route
stockRoutes.route('/').get(function (req, res) {
  Stock.find(function (err, stocks) {
    if (err) {
      console.log(err);
    } else {
      res.json(stocks);
    }
  });
});


module.exports = stockRoutes;
