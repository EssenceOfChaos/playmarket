const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const market = require('./data/market');
const portfolioRoutes = require('./routes/portfolio.route');
const userRoutes = require('./routes/user.route');
const pkg = require('../package.json');

const port = process.env.PORT || 3000;

// const auth0Token = require('./auth0');

// console.log(auth0Token);

mongoose.Promise = global.Promise;
mongoose.connect(config.DB, { useNewUrlParser: true, useUnifiedTopology: true }).then(
  () => {
    console.log('Database is connected');
  },
  err => {
    console.log('Can not connect to the database' + err);
  }
);

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});


app.get('/api/market', (req, res) => {
  res.send(market.marketPositions);
});

app.get('/version', (req, res) => {
  res.send(pkg.version);
});

app.use('/portfolios', portfolioRoutes);
app.use('/users', userRoutes);


const server = app.listen(port, function () {
  console.log('express server listening on port ' + port);
});

module.exports.sever = server;
