export const environment = {
  production: true,
  IEX_PK: 'pk_40c6c71966a445cca7038a5445fd54a0',
  IEX_BASE_URL: 'https://cloud.iexapis.com/stable',
  PORTFOLIO_URI: 'http://localhost:3000/portfolios',
  USER_URI: 'http://localhost:3000/users',
  STOCK_URI: 'http://localhost:3000/stocks',
  SANDBOX_API: 'https://sandbox.iexapis.com/stable',
  PRODUCTION_API: 'https://cloud.iexapis.com/stable'
};
