// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  IEX_PK: 'Tpk_e370cc9230a611e9958142010a80043c',
  IEX_BASE_URL: 'https://sandbox.iexapis.com/stable',
  PORTFOLIO_URI: 'http://localhost:3000/portfolios',
  USER_URI: 'http://localhost:3000/users',
  STOCK_URI: 'http://localhost:3000/stocks',
  SANDBOX_API: 'https://sandbox.iexapis.com/stable',
  PRODUCTION_API: 'https://cloud.iexapis.com/stable'
};

// BASE_URL: 'https://cloud.iexapis.com/'
// followed by version: i.e. 'stable' or 'latest'
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
