import { Component, OnInit } from '@angular/core';

import { AuthService } from './auth/auth.service';
import { StockService } from 'src/app/shared/stock.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public watchList;

  constructor(private auth: AuthService, public stockService: StockService) {}

  ngOnInit() {
    console.log('[AppComponent:] ngOnInit has fired!');
    // On initial load, check authentication state with authorization server
    // Set up local auth streams if user is already authenticated
    this.auth.localAuthSetup();
    const res = this.stockService.getWatchlist();
    this.watchList = JSON.parse(res);
  }
}
