export class User {
  name = '';
  email = '';
  logged_in = false;
  email_verified: boolean;
  picture: string;
  sub: string;
  updated_at: Date;
  nickname?: string | undefined;
  has_portfolio?: boolean;
}

// email: "fjschiller@gmail.com"
// email_verified: true
// https://example.com/roles: ["admin"]
// name: "fjschiller@gmail.com"
// nickname: "fjschiller"
// picture: "https://s.gravatar.com/avatar/4dbadea21fef90ad5339fdeab464ecde?s=480&r=pg&d=https%3A%2F%2Fi.imgur.com%2Fc7pGAvA.png"
// sub: "auth0|5a8cf0b35c679b17811059e4"
// updated_at: "2019-09-27T21:58:37.871Z"
// __proto__: Object
