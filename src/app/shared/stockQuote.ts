export interface StockQuote {
  symbol: string;
  companyName: string;
  primaryExchange: string;
  calculationPrice: string;
  open: number;
  openTime: number;
  close: number;
  closeTime: number;
  high: number;
  low: number;
  latestPrice: number;
  latestSource: string;
  latestTime: string;
  latestUpdate: number;
  latestVolume: number;
  previousClose: number;
  previousVolume: number;
  change: number;
  changePercent: number;
  marketCap: number;
  peRatio: number;
  week52High: number;
  week52Low: number;
  ytdChange: number;
  lastTradeTime: number;
}

// Return of Stock Quote
// {
//   'symbol': "AAPL",
//   'companyName':'Apple, Inc.',
//   'primaryExchange': "NASDAQ",
//   'calculationPrice':'close',
//   'open': 206,
//   'openTime': 1563543000745,
//   'close': 202.59,
//   'closeTime': 1563566400618,
//   'high': 206.5,
//   'low': 202.36,
//   'latestPrice': 202.59,
//   'latestSource': "Close",
//   'latestTime':'July 19, 2019',
//   'latestUpdate': 1563566400618,
//   'latestVolume': 20895587,
//   'iexRealtimePrice': null,
//   'iexRealtimeSize': null,
//   'iexLastUpdated': null,
//   'delayedPrice': 202.52,
//   'delayedPriceTime': 1563567300002,
//   'extendedPrice': 202.4,
//   'extendedChange': -0.19,
//   'extendedChangePercent': -0.00094,
//   'extendedPriceTime': 1563667192141,
//   'previousClose': 205.66,
//   'previousVolume': 18582161,
//   'change': -3.07,
//   'changePercent': -0.01493,
//   'volume': 20895587,
//   'iexMarketPercent': null,
//   'iexVolume': null,
//   'avgTotalVolume': 21782284,
//   'iexBidPrice': null,
//   'iexBidSize': null,
//   'iexAskPrice': null,
//   'iexAskSize': null,
//   'marketCap': 932132797200,
//   'peRatio': 16.9,
//   'week52High': 233.47,
//   'week52Low': 142,
//   'ytdChange': 0.267935,
//   'lastTradeTime': 1563567300002;
// }
