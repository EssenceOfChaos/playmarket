import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

// import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AlertService {
  observer = new Subject();

  public subscriber$ = this.observer.asObservable();

  constructor() {}
  notifyUser(msg) {
    this.observer.next(msg);
  }
}
