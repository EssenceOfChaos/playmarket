import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse
} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { User } from '../models/user';

const current_user: User = new User();

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private http: HttpClient) {}

  updateState(user, loggedIn) {
    current_user.name = user.name;
    current_user.email = user.email;
    current_user.email_verified = user.email_verified;
    current_user.picture = user.picture;
    current_user.sub = user.sub;
    current_user.logged_in = loggedIn;
    current_user.updated_at = user.updated_at;
    current_user.nickname = user.nickname;

    // console.log(current_user);
  }

  returnCurrentUser() {
    return current_user;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }
}
