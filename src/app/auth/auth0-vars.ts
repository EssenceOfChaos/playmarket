interface AuthConfig {
  clientID: string;
  domain: string;
  callbackURL: string;
  scope: string;
  databaseURL: string;
  userByEmailURL: string;
}

export const AUTH_CONFIG: AuthConfig = {
  clientID: 'W6bzsrKp5QaeIxtNgFBnMyLMX5kr9ipE',
  domain: 'swiftlabs.auth0.com',
  callbackURL: 'http://localhost:4200/callback',
  scope: 'openid profile',
  databaseURL: '/dbconnections/signup',
  userByEmailURL:
    'https://swiftlabs.auth0.com/api/v2/users-by-email?fields=email_verified'
};
