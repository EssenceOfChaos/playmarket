import { AfterViewInit, Component, OnInit } from '@angular/core';

import { AlertService } from 'src/app/shared/alert.service';
import { ClrDatagridSortOrder } from '@clr/angular';
import { STOCKS } from '../marketStocks';
// import { StaticSymbol } from '@angular/compiler';
import { StockService } from 'src/app/shared/stock.service';
import { Title } from '@angular/platform-browser';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/shared/user.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit, AfterViewInit {
  pageTitle = 'PlayMarket Stocks';
  descSort = ClrDatagridSortOrder.DESC;
  selectedStocks = [];
  isLoading = true;
  stockList = new Map();
  stockNames = STOCKS;

  userProfile;
  store = window.localStorage;
  // currentUser = document.getElementById('profileEmail').innerHTML;
  currentUser: User;

  constructor(
    private stockService: StockService,
    public userService: UserService,
    public alertService: AlertService,
    private title: Title
  ) {}

  ngOnInit() {
    console.log('[PortfolioComponent]: ngOnInit has fired!');
    this.isLoading = false;
    this.title.setTitle(this.pageTitle);
    this.getBatchQuote(Object.keys(STOCKS));
  }

  ngAfterViewInit() {
    console.log('[PortfolioComponent]: ngAfterViewInit has fired!');
    this.currentUser = this.userService.returnCurrentUser();
  }

  /**
   * Takes in a stock symbol and returns the price.
   * @param stockSymbol - the symbol of the stock to lookup
   */
  getStockPrice(stockSymbol: string) {
    this.stockService
      .getPrice(stockSymbol)
      .subscribe((price) => console.log(price));
  }

  /**
   * Retreives the stock prices for a list of stocks
   * @param stocks - the list of stocks from marketStocks.ts
   */
  getBatchQuote(stocks) {
    this.stockService.batchQuote(stocks).subscribe((res) => {
      const batchQuoteStocks = Object.entries(res);
      for (const entry of batchQuoteStocks) {
        this.stockList.set(entry[0], entry[1].price);
      }
    });
  }

  createPortfolio(userSelections) {
    this.stockService.addPortfolio(this.currentUser.email, userSelections);
    // userSelections is an object of objects with key -> stockSymbol and value -> stockPrice
  }
  removePortfolio(portfolioId) {
    // call the stock service 'removePortfolio/1' function passing in the portfolio _id
  }

  addWatchlist(symbol, price) {
    const stock = { key: symbol, value: price };
    // this.stockService.watchStock(stock);
    console.log(stock);
  }

  selectionChanged(event) {
    if (this.selectedStocks.length == 5) {
      console.log(
        'you have selected the maximum number of stocks available for a basic portfolio.'
      );
      this.sendMaxStocksAlert();
    }
    console.log(event);
  }

  printSelectedStocks() {
    console.log(this.selectedStocks);
  }

  sendMaxStocksAlert() {
    this.alertService.notifyUser(
      'You have selected the maximum (5) number of stocks!'
    );
  }
}
