import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarketRoutingModule } from './market-routing.module';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';

@NgModule({
  declarations: [PortfolioComponent],
  imports: [CommonModule, MarketRoutingModule, FormsModule, ClarityModule]
})
export class MarketModule {}
