import { AfterViewInit, Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from 'src/app/auth/auth.service';
import { StockService } from '../../shared/stock.service';
import { Title } from '@angular/platform-browser';
import { UserService } from '../../shared/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, AfterViewInit {
  pageTitle = 'PlayMarket Home';
  message = 'Welcome to PlayMarket!';
  currentUser;
  userPortfolio;
  currentPortfolioValue;
  apiStatus;
  chartData = { data: [], label: 'My First Chart' };
  chartLabels = [];
  showChart = false;
  form: FormGroup;
  lineChartLabels: Label[] = [
    'June',
    'July',
    'August',
    'September',
    'October',
    'November'
  ];

  constructor(
    public auth: AuthService,
    public userService: UserService,
    public stockService: StockService,
    private formBuilder: FormBuilder,
    private title: Title
  ) {}
  ngOnInit() {
    this.auth.isAuthenticated$.subscribe((result) => {
      console.log('user logged in:', result);
    });
    this.currentUser = this.userService.returnCurrentUser();
    this.stockService.getStatus().subscribe((res) => (this.apiStatus = res));
    this.title.setTitle(this.pageTitle);
  }

  ngAfterViewInit() {
    if (this.currentUser.logged_in) {
      this.getUserPortfolio();
      this.getStockUpdates();
    }
  }

  submit() {
    console.log('reactive form submit', this.form.value);
  }
  getUserPortfolio() {
    this.stockService
      .getUserPortfolio(this.currentUser.name)
      .subscribe((results) => {
        const stocks = results[0].stocks;
        this.userPortfolio = stocks;
      });
  }

  getStockUpdates() {
    if (this.userPortfolio) {
      console.log(this.userPortfolio);
      const updates = [];

      for (const stock of this.userPortfolio) {
        updates.push(stock.key);
      }

      this.stockService.batchQuote(updates).subscribe((results) => {
        console.log(results);
      });
    }
  }

  getStockChart(sym) {
    this.stockService.getStockChart(sym).subscribe((res) => {
      const arr = Object.values(res);
      arr.forEach((day) => {
        this.chartData.data.push(day.close);
        this.chartLabels.push(day.date);
      });
      console.log(res);
    });
    this.showChart = true;
  }
}
