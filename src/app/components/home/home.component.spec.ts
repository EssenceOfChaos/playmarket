import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { AuthService } from 'src/app/auth/auth.service';
import { HomeComponent } from './home.component';
import { Router } from '@angular/router';
import { StockService } from '../../shared/stock.service';
import { UserService } from '../../shared/user.service';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent],
      providers: [AuthService, StockService, UserService, Router]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
