import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/auth/auth.service';
// import { Observable } from 'rxjs';
// import { FormControl, FormGroup } from '@angular/forms';
// import { StockService } from 'src/app/shared/stock.service';
import { Title } from '@angular/platform-browser';

// import { UserService } from 'src/app/shared/user.service';

// import { User } from 'src/app/models/user';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  // users: Observable<User[]>;
  pageTitle = 'Profile Page';
  profileJson: string = null;
  userPortfolio;
  currentUser = JSON.parse(window.localStorage.getItem('currentUser'));
  loggedIn = window.localStorage.getItem('loggedIn');

  constructor(public auth: AuthService, private title: Title) {}

  ngOnInit() {
    if (this.auth.loggedIn) {
      console.log('{auth.loggedIn: true}');
    }
    this.title.setTitle(this.pageTitle);

    this.auth.userProfile$.subscribe(
      (profile) => (this.profileJson = JSON.stringify(profile, null, 2))
    );

    if (this.currentUser) {
      console.log(this.currentUser.name);
    }
  }
}
