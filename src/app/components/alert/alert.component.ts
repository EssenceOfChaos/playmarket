import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output
} from '@angular/core';

import { AlertService } from 'src/app/shared/alert.service';

interface Message {
  type: string;
  content: String;
}
@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnDestroy {
  @Output() undoButton = new EventEmitter();
  msg;

  constructor(public alertService: AlertService) {}

  ngOnInit(): void {
    this.alertService.subscriber$.subscribe((data) => {
      this.msg = data;
      console.log(data);
    });
  }

  ngOnDestroy() {
    console.log('[AlertComponent] ngOnDestroy called.');
  }

  alertOnClose() {
    this.msg = '';
  }
}
