import { Component, OnInit } from '@angular/core';

import { AlertService } from 'src/app/shared/alert.service';
import { STOCKS } from 'src/app/modules/market/marketStocks';
import { Sector } from 'src/app/shared/sector';
import { StockQuote } from 'src/app/shared/stockQuote';
import { StockService } from 'src/app/shared/stock.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  pageTitle = 'Dashboard';
  stockInput = '';
  stockQuote: StockQuote;
  sectors: Sector[];
  showSectors = false;
  watchList = [];
  stocks = STOCKS;
  stockList;
  showModal = false;
  localStore = window.localStorage;

  constructor(
    private stockService: StockService,
    private title: Title,
    private alertService: AlertService
  ) {}

  ngOnInit() {
    console.log('DashboardComponent: ngOnInit has fired!');
    this.title.setTitle(this.pageTitle);
    // this.getSectorPerformance();
    this.getBatchQuote();
  }

  getStockQuote(stockTicker: string) {
    console.log(stockTicker);
    this.stockService
      .getQuote(stockTicker)
      .subscribe((quote) => (this.stockQuote = quote));
  }

  getBatchQuote() {
    const stocks = Object.keys(this.stocks);
    this.stockService.batchQuote(stocks).subscribe((res) => {
      this.stockList = res;
      console.log(this.stockList);
    });
  }

  getSectorPerformance() {
    this.stockService
      .getSectorPerformance()
      .subscribe((res) => (this.sectors = res));
    // console.log(this.sectors);
  }

  getMoreInfo(stockTicker) {
    console.log(stockTicker);
    this.getStockQuote(stockTicker);
    this.showModal = true;
  }

  addToWatchlist(sym) {
    console.log(sym);
    this.watchList.push(sym);
    this.localStore.setItem('watchlist', JSON.stringify(this.watchList));
    this.alertService.notifyUser(
      `${sym} has been successfully added to your Watchlist!`
    );
  }

  removeFromWatchlist() {
    // this.watchList.pop();
    // this.localStore.setItem('watchlist', JSON.stringify(this.watchList));
  }

  logWatchStocks() {
    console.log(this.watchList);
  }
}
