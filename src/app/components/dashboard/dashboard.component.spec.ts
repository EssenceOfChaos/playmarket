import { ComponentFixture, TestBed, async } from '@angular/core/testing';

import { AlertService } from 'src/app/shared/alert.service';
import { DashboardComponent } from './dashboard.component';
import { StockService } from 'src/app/shared/stock.service';
import { Title } from '@angular/platform-browser';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      providers: [StockService, Title, AlertService]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
